using UnityEngine;

public class PurchaseButton : MonoBehaviour
{
    public enum PurchaseType { removeAds, coin100, coin500 }
    public PurchaseType purchaseType;

    public void ClickPurchaseButton()
    {
        switch (purchaseType)
        {
            case PurchaseType.removeAds:
                IAPManager.instance.BuyRemoveAds();
                break;
            case PurchaseType.coin100:
                IAPManager.instance.BuyCoin100();
                break;
            case PurchaseType.coin500:
                IAPManager.instance.BuyCoin500();
                break;
        }
    }
}
