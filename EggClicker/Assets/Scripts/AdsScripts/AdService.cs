using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Monetization;

namespace GameServices 
{
    public class AdService : MonoBehaviour ,IService
    {
        private string iOSGameID = "4007782";
        private string placementID_NonRewarded = "video";
        private string placementID_Rewarded = "rewardedVideo";
        private bool testMode = true;
        public void Initialize()
        {
#if UNITY_IOS || UNITY_ANDROID
            Monetization.Initialize(iOSGameID,testMode);
            DisplayNonRewardedAd();

#else
            Debug.LogWarning("Current platform doesnt support unity monetization");
#endif
        }

        public void DisplayNonRewardedAd() 
        {
            if (!Monetization.IsReady(placementID_NonRewarded)) 
            {

                Debug.LogWarningFormat("Placement <{0}> not ready to display.",placementID_NonRewarded);
                return;
            }

            ShowAdPlacementContent adcontent = Monetization.GetPlacementContent(placementID_NonRewarded) as ShowAdPlacementContent;

            if (adcontent != null)
                adcontent.Show();
            else
                Debug.LogWarning("Placement video returne null");
        }
        public void DisplayRewardedAd() 
        {
            StartCoroutine(waitanddisplayrewardedad());
        }

        public IEnumerator waitanddisplayrewardedad()
        {
            yield return new WaitUntil(() => Monetization.IsReady(placementID_Rewarded));

            ShowAdPlacementContent adContent = Monetization.GetPlacementContent(placementID_Rewarded) as ShowAdPlacementContent;
            adContent.gamerSid = "[CALLBACK SERVER ID HERE]";

            ShowAdCallbacks callbacksOptions = new ShowAdCallbacks();
            callbacksOptions.finishCallback += RewardCallback;

            if (adContent != null)
            {
                adContent.Show(callbacksOptions);
            }
            else 
            {
                Debug.LogWarning("Placement content returned null.");
            }

        }
        private void RewardCallback(ShowResult result)
        {
            switch(result)
            {
                case ShowResult.Finished:
                    Debug.Log("Rewards for player");
                    break;
                case ShowResult.Skipped:
                    Debug.Log("Ad skipped");
                    break;
                case ShowResult.Failed:
                    Debug.Log("Ad failed");
                    break;
            }
        }

    }
}
