using System;
using UnityEngine;

public class DDOL : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        
#if UNITY_EDITOR
        Debug.unityLogger.logEnabled = true;
        Debug.Log("DDOL " + gameObject.name);
#else
        Debug.unityLogger.logEnabled = false;
#endif
    }
}
