﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class SaveableEntity : MonoBehaviour {                                   // Component for making GameObject saveable with a unique Id 
    [SerializeField] private string id = string.Empty;

    public string Id => id;

    [ContextMenu("Generate Id")]
    private void GenerateId() => id = Guid.NewGuid().ToString();
    
    public object CaptureState() {                                              // Store Data from all components using ISaveable interface
        var state = new Dictionary<string, object>();                           // String = name of data component, Object = data values 

        var saveableList = GetComponents<ISaveable>();

        foreach(var saveable in saveableList) {
            state[saveable.GetType().ToString()] = saveable.CaptureState();
        }
        return state;
    }

    public void RestoreState(object state) {                                    // Override Data in all components using ISaveable
        var stateDictionary = (Dictionary<string, object>)state;

        var saveableList = GetComponents<ISaveable>();

        foreach (var saveable in saveableList) {
            string typeName = saveable.GetType().ToString();

            if(stateDictionary.TryGetValue(typeName, out object value)) {       
                saveable.RestoreState(value);
            }
        }
    }
}
