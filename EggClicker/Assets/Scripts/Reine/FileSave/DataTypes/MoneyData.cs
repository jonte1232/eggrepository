using System;
using UnityEngine;

[RequireComponent(typeof(SaveableEntity))]
public class MoneyData : MonoBehaviour, ISaveable
{
    
    public float _money = 0;
    public float _totalMoney;
    public float _moneySpent;
    public float _moneyPerSecond;

    // Store data values for saving
    public object CaptureState()
    {
        var data = new SaveData();
        data.Money = _money;
        data.TotalMoney = _totalMoney;
        data.MoneySpent = _moneySpent;
        data.MoneyPerSecond = _moneyPerSecond;
        return data;
    }

    // Get data values for loading
    public void RestoreState(object state) 
    {
        var saveData = (SaveData)state;
        
        _money = saveData.Money;
        _totalMoney = saveData.TotalMoney;
        _moneySpent = saveData.MoneySpent;
        _moneyPerSecond = saveData.MoneyPerSecond;
    }

    [Serializable]
    public struct SaveData 
    {
        public float Money;
        public float TotalMoney;
        public float MoneySpent;
        public float MoneyPerSecond;
    }
}