using System;
using UnityEngine;

[RequireComponent(typeof(SaveableEntity))]
public class ProfileData : MonoBehaviour, ISaveable
{
    public int _dateYear;
    public int _dateMonth;
    public int _dateDay;
    public int _dateHour;
    public int _dateMinute;
    public int _dateSecond;

    [Header("Playtime in seconds")]
    public int _activePlayTime;
    public int _totalPlayTime;
    public int _lastSaveTotalPlaytime;
    public int _secondsOffline;

    // Store data values for saving
    public object CaptureState()
    {
        var data = new SaveData();
        data.DateYear = _dateYear;
        data.DateMonth = _dateMonth;
        data.DateDay = _dateDay;
        data.DateHour = _dateHour;
        data.DateMinute = _dateMinute;
        data.DateSecond = _dateSecond;
        data.ActivePlayTime = _activePlayTime;
        data.TotalPlaytime = _totalPlayTime;
        data.LastSaveTotalPlaytime = _lastSaveTotalPlaytime;
        data.SecondsOffline = _secondsOffline;
        return data;
    }

    // Get data values for loading
    public void RestoreState(object state) 
    {
        var saveData = (SaveData)state;
        
        _dateYear = saveData.DateYear;
        _dateMonth = saveData.DateMonth;
        _dateDay = saveData.DateDay;
        _dateHour = saveData.DateHour;
        _dateMinute = saveData.DateMinute;
        _dateSecond = saveData.DateSecond;
        _activePlayTime = saveData.ActivePlayTime;
        _totalPlayTime = saveData.TotalPlaytime;
        _lastSaveTotalPlaytime = saveData.LastSaveTotalPlaytime;
        _secondsOffline = saveData.SecondsOffline;
    }

    [Serializable]
    public struct SaveData
    {
        public int DateYear;
        public int DateMonth;
        public int DateDay;
        public int DateHour;
        public int DateMinute;
        public int DateSecond;
        public int ActivePlayTime;
        public int TotalPlaytime;
        public int LastSaveTotalPlaytime;
        public int SecondsOffline;
    }
}