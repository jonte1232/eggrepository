using System;
using UnityEngine;

[RequireComponent(typeof(SaveableEntity))]
public class BuildingData : MonoBehaviour, ISaveable
{
    public int _tentTimesPurchased;
    public int _helperTimesPurchased;
    public int _houseTimesPurchased;

    // Store data values for saving
    public object CaptureState()
    {
        var data = new SaveData();
        data.TentTimesPurchased = _tentTimesPurchased;
        data.HelperTimesPurchased = _helperTimesPurchased;
        data.HouseTimesPurchased = _houseTimesPurchased;
        return data;
    }

    // Get data values for loading
    public void RestoreState(object state) 
    {
        var saveData = (SaveData)state;
        
        _tentTimesPurchased = saveData.TentTimesPurchased;
        _helperTimesPurchased = saveData.HelperTimesPurchased;
        _houseTimesPurchased = saveData.HouseTimesPurchased;
    }

    [Serializable]
    public struct SaveData 
    {
        public int TentTimesPurchased;
        public int HelperTimesPurchased;
        public int HouseTimesPurchased;

    }
}