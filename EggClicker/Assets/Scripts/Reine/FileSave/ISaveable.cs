﻿using System.Collections;

// Saveable Interface for creating Data Components
public interface ISaveable {
    object CaptureState();
    void RestoreState(object state);
}

/*  Data Component Example
 *  --------------------------------------------------------------------------
 * 
 *  [RequireComponent(typeof(SaveableEntity))]
 *  class ExampleData : Monobehaviour, ISaveable {
 *  
 *      private var exampleDataVariable = 100;
 *      
 *      public object CaptureState() {
 *          return new SaveData {
 *              ExampleDataVariable = exampleDataVariable;
 *          };
 *      }
 *      
 *      public void RestoreState(object state) {
 *          var saveData = (SaveData)state;
 *          
 *          exampleDataVariable = saveData.ExampleDataVariable;
 *      }
 *      
 *      [Serializable]
 *      private struct SaveData {
 *          public var ExampleDataVariable;
 *      }
 *  }
 */
