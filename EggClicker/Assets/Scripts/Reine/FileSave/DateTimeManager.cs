using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DateTimeManager : MonoBehaviour
{
    [SerializeField] private ProfileData profileData = null;
    [SerializeField] private MoneyData moneyData = null;

    public DateTime CreationDateTime
    {
        get => _creationDateTime;
        private set => _creationDateTime = value;
    }

    private DateTime _creationDateTime;

    //----------------------------------------------------------------------------------------------------------

    private void Start()
    {
        StartCoroutine(CountSeconds());
    }

    // Handle ProfileData and MoneyData when loading from file
    #region Save/Load

    // Serialize profile creation date
    // DO THIS ONLY ONCE WHEN CREATING A NEW SAVE FILE!!!
    public void PackDateTime()
    {
        if (profileData._dateYear != 0)
        {
            return;
        }
        
        _creationDateTime = DateTime.Now;
        
        profileData._dateYear = _creationDateTime.Year;
        profileData._dateMonth = _creationDateTime.Month;
        profileData._dateDay = _creationDateTime.Day;
        profileData._dateHour = _creationDateTime.Hour;
        profileData._dateMinute = _creationDateTime.Minute;
        profileData._dateSecond = _creationDateTime.Second;
    }

    // Restore creation date
    // Do this on Load
    public void UnpackDateTime()
    {
        if (profileData._dateYear == 0)
        {
            _creationDateTime = DateTime.Now;
        }
        else
        {
            _creationDateTime = new DateTime(profileData._dateYear, profileData._dateMonth, profileData._dateDay,
                profileData._dateHour, profileData._dateMinute, profileData._dateSecond);
        }
    }

    // Set TotalPlaytime to (creation DateTime - current DateTime)
    // Do this on Load
    public void SetTotalPlaytime()
    {
        profileData._lastSaveTotalPlaytime = profileData._totalPlayTime;
        
        var totalSeconds = (DateTime.Now - _creationDateTime).TotalSeconds;
        profileData._totalPlayTime = (int) totalSeconds;

        profileData._secondsOffline = profileData._totalPlayTime - profileData._lastSaveTotalPlaytime;
    }

    public void SetOfflineMoneyEarned()
    {
        
    }

    #endregion

    // Count up in seconds for ProfileData.ActivePlaytime & ProfileData.TotalPlaytime (& save data)
    #region SecondsCounter
    
    private IEnumerator CountSeconds()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            profileData._activePlayTime += 1;
            profileData._totalPlayTime += 1;
            DataSaver.Instance.Save();
        }
    }

    #endregion
    
}
