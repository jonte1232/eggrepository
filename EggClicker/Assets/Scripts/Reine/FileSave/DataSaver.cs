﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class DataSaver : MonoBehaviour {                                                    // COMPONENT FOR GAME MANAGER
                                                                                            //-----------------------------------------------------------------------------------------------------
    [Header("References")]
    [SerializeField] private DateTimeManager _dateTimeManager = null;
    [SerializeField] private Tent _tent = null;
    [SerializeField] private House _house = null;
    [SerializeField] private DwarfHelper _dwarfHelper = null;

    [Header("Data References")]
    [SerializeField] private BuildingData _buildingData = null;
    [SerializeField] private MoneyData _moneyData = null;
    [SerializeField] private ProfileData _profileData = null;
                                                                                            // Store and Override Data in all GameObjects using 'SaveableEntity' component
                                                                                            
    private string _savePath => $"{Application.persistentDataPath}/ProfileData.txt";        // Path / filename of Save file

    #region Singleton

    private static DataSaver _instance;

    public static DataSaver Instance { get { return _instance; } }

    #endregion
    //----------------------------------------------------------------------------------------------------------------------

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
        
        Load();
    }

    //----------------------------------------------------------------------------------------------------------------------
    [ContextMenu("Save")]                                                                               
    public void Save() {
        var state = LoadFile();                                            // Create new Dictionary with Data from file. If there is no Save file, Create an empty Dictionary for Data storage
        CaptureState(state);
        SaveFile(state);
    }

    [ContextMenu("Load")]
    public void Load() {
        var state = LoadFile();
        RestoreState(state);
        
        // Set playtime
        _dateTimeManager.UnpackDateTime();
        _dateTimeManager.SetTotalPlaytime();
        
        // Reset Money Spent
        _moneyData._money += _moneyData._moneySpent;
        _moneyData._moneySpent = 0;
        
        // Get offline moneyPerSecond
        GetOfflineMoney();

        StartCoroutine(DelayLoad());
    }

    //----------------------------------------------------------------------------------------------------------------------
    private void SaveFile(object state) {                                                   // Create or Overwrite the Save file
        using (var stream = File.Open(_savePath, FileMode.Create)) {
            var formatter = new BinaryFormatter();
            formatter.Serialize(stream, state);
        }
    }

    private Dictionary<string, object> LoadFile() {                                         // Load Data from the Save file
        _dateTimeManager.PackDateTime();
        
        if (!File.Exists(_savePath)) {
            return new Dictionary<string, object>();
        }

        using(FileStream stream = File.Open(_savePath, FileMode.Open)) {
            var formatter = new BinaryFormatter();
            return (Dictionary<string, object>)formatter.Deserialize(stream);
        }
    }

    public void CaptureState(Dictionary<string, object> state) {                           // Store Data from all GameObjects using 'SaveableEntity' component

        var saveableEntityList = FindObjectsOfType<SaveableEntity>();

        foreach (var saveable in saveableEntityList) {
            state[saveable.Id] = saveable.CaptureState();
        }
    }

    public void RestoreState(Dictionary<string, object> state) {                           // Override Data in all GameObjects using 'SaveableEntity' component

        var saveableEntityList = FindObjectsOfType<SaveableEntity>();
        foreach (var saveable in saveableEntityList) {
            if (state.TryGetValue(saveable.Id, out object value)) {
                saveable.RestoreState(value);
            }
        }
    }
    //----------------------------------------------------------------------------------------------------------------------

    private void GetOfflineMoney()
    {
        _moneyData._money += (_moneyData._moneyPerSecond * _profileData._secondsOffline);
        _moneyData._totalMoney += (_moneyData._moneyPerSecond * _profileData._secondsOffline);
    }
    
    private IEnumerator DelayLoad()
    {
        yield return new WaitForSeconds(.1f);

        var tent = _buildingData._tentTimesPurchased;
        var house = _buildingData._houseTimesPurchased;
        var helper = _buildingData._helperTimesPurchased;
        
        // Set Buildings purchased
        for (int i = 0; i < tent; i++)
        {
            _tent.BuyBuilding();
        }
        
        for (int i = 0; i < house; i++)
        {
            _house.BuyBuilding();
        }
        
        for (int i = 0; i < helper; i++)
        {
            _dwarfHelper.BuyBuilding();
        }
    }
}
