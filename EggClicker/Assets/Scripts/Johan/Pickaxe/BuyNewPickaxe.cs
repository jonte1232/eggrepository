using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyNewPickaxe : MonoBehaviour
{
    [SerializeField] List<Transform> pickaxes = new List<Transform>();
    bool allPurchased;
    ClicksScript clicksScript;
    CoinHandler coinHandler;
    Button button;
    int cost = 101;
    int counter;
    AudioSource clip;
    private void Start()
    {
        clicksScript = FindObjectOfType<ClicksScript>();
        coinHandler = FindObjectOfType<CoinHandler>();
        button = GetComponent<Button>();
        clip = GetComponent<AudioSource>();
    }

    public void BuyNextPickaxe()
    {
        if(allPurchased == false && coinHandler.Coins >= cost)
        {
            switch (counter)
            {
                case 0:
                    pickaxes[0].gameObject.SetActive(false);
                    pickaxes[1].gameObject.SetActive(true);
                    pickaxes[2].gameObject.SetActive(false);
                    counter++;
                    coinHandler.Coins = coinHandler.Coins - cost;
                    clicksScript.MPSMultiplier += 0.05f;
                    clicksScript.UpdateMoneyPerClick();
                    clip.Play();
                    break;
                case 1:
                    pickaxes[0].gameObject.SetActive(false);
                    pickaxes[1].gameObject.SetActive(false);
                    pickaxes[2].gameObject.SetActive(true);
                    coinHandler.Coins = coinHandler.Coins - cost;
                    button.interactable = false;
                    allPurchased = true;
                    clicksScript.MPSMultiplier += 0.1f;
                    clicksScript.UpdateMoneyPerClick();
                    clip.Play();
                    break;
                default:
                    break;
            }
        }
    }
}
