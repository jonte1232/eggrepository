using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public delegate void ClickAction();
public class ClicksScript : MonoBehaviour
{
    public event ClickAction onClickEgg;
    BuildingsManager buildingsManager;
    public int _clicks = 0;
    public Text text;
    private float _moneyPerClick = 1;
    private float _clickAdditiveBonus = 0;
    private float _MPSMultiplier = 0;
    private float _MPSBonus = 1;
    Vector3 transformScale;

    public RectTransform floatingTextPrefab;
    [SerializeField] Animator anim;
    public float MPSMultiplier
    {
        get { return _MPSMultiplier; }
        set { _MPSMultiplier = value; }
    }
    public float MoneyPerClick
    {
        get { return _moneyPerClick; }
        set { _moneyPerClick = value; }
    }
    public float ClickAdditiveBonus
    {
        get { return _clickAdditiveBonus; }
        set { _clickAdditiveBonus = value; }
    }
    public float MPSBonus { get => _MPSBonus; set => _MPSBonus = value; }
    private void Start()
    {
        transformScale = transform.localScale;
        buildingsManager = FindObjectOfType<BuildingsManager>();
        Publisher.OnBuyBuilding += UpdateMoneyPerClick;
        Publisher.OnUpgradePurchase += UpdateMoneyPerClick;
    }
    // Start is called before the first frame update
    public int Clicks
    {
        get => _clicks;
        set => _clicks = value;
    }
    

    public void ClickEgg()
    {
        _clicks++;
        text.text = "Clicks: " + _clicks.ToString();
        anim.SetTrigger("Swing");
        if (onClickEgg != null)
        {
            onClickEgg();
        }
        Instantiate(floatingTextPrefab.gameObject, transform);
    }

    public void UpdateMoneyPerClick()
    {
        if(MPSMultiplier > 0)
        {
            MPSBonus = BuildingsManager.moneyPerSecond * MPSMultiplier;
            MoneyPerClick = 1 + ClickAdditiveBonus + MPSBonus;
        }
        else
        {
            MoneyPerClick = 1 + ClickAdditiveBonus;
        }
        
    }
}
