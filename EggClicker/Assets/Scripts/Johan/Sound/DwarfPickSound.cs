using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DwarfPickSound : MonoBehaviour
{
    AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void playClip()
    {
        audioSource.Play();
    }
}
