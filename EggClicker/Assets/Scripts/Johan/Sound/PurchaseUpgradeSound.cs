using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurchaseUpgradeSound : MonoBehaviour
{
    [SerializeField]AudioSource clip;

    private void Start()
    {
        Publisher.OnUpgradePurchase += PlayClip;
    }
    void PlayClip()
    {
        clip.Play();
    }
}
