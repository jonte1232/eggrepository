using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyBuildingSound : MonoBehaviour
{
    [SerializeField] AudioSource clip;

    private void Start()
    {
        Publisher.OnBuyBuilding += PlayClip;
    }
    void PlayClip()
    {
        clip.Play();
    }
}
