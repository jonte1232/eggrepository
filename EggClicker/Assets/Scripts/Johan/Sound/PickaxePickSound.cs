using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickaxePickSound : MonoBehaviour
{
    AudioSource clip;
    // Start is called before the first frame update
    void Start()
    {
        clip = GetComponent<AudioSource>();
    }

    void PlayClip()
    {
        clip.pitch = Random.Range(0.95f, 1.05f);
        clip.Play();
    }
}
