using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyClickGainScroll : MonoBehaviour
{
    [SerializeField]ClicksScript clicks;
    Text text;
    [SerializeField] float scrollSpeed;
    private void Start()
    {
        clicks = FindObjectOfType<ClicksScript>();
        text = GetComponent<Text>();
        text.text = "+" + clicks.MoneyPerClick.ToString();
        transform.position = new Vector3(transform.position.x + Random.Range(-10,10), transform.position.y, transform.position.z);
        Invoke("DestroyText", 1.5f);
    }
    private void Update()
    {
        transform.Translate(Vector3.up * scrollSpeed * Time.deltaTime);
    }

    private void DestroyText()
    {
        Destroy(this.gameObject);
    }
}
