using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum UnlockRequirement
{
    CurrentMoneyAmount,
    TotalMoneyAmount,
    ClicksAmount,
    BuildingTimesPurchased,
    TimeSpentOnline,
    TimeSpentOffline
}
public class UpgradesScript : MonoBehaviour
{
    #region Variables
    public int cost;
    public string upgradeName;
    public string upgradeInfo;

    ClicksScript clicksScript;
    [HideInInspector]public MoneyScript moneyScript;
    [HideInInspector]public Button thisButton;
    Publisher publisher;
    protected Text text;
    protected bool unlocked = false;
    [Space]
    //public ClassType selectedUpgradeType;
    [HideInInspector]public Buildings building;
    [Space]
    public UnlockRequirement unlockRequirement;
    protected BuyUpgradeHandler buyHandler;
    public int amountToUnlockRequirement;
    #endregion

    protected virtual void Awake()
    {
        publisher = FindObjectOfType<Publisher>();
        text = GetComponentInChildren<Text>();
        clicksScript = FindObjectOfType<ClicksScript>();
        moneyScript = FindObjectOfType<MoneyScript>();
        thisButton = GetComponent<Button>();
        text.text = upgradeName;
        AddThisUpgradeToUpgradesList();
        GetBuildingType();
        this.gameObject.SetActive(false);
    }
    private void Start()
    {
        buyHandler = FindObjectOfType<BuyUpgradeHandler>();
    }

    protected virtual void GetBuildingType()
    {
        /*
        switch (selectedUpgradeType)
        {
            case ClassType.None:
                Debug.LogError("No Upgrade Type was selected on " + thisButton.ToString());
                break;
            case ClassType.AutoClicker:
                building = FindObjectOfType<AutoClicker>();
                break;
            case ClassType.DwarfHelper:
                building = FindObjectOfType<DwarfHelper>();
                break;
            case ClassType.MiningFacility:
                building = FindObjectOfType<MiningFacility>();
                break;
            default:
                break;
        }
        */
    }

    public virtual void Unlock()
    {
        switch (unlockRequirement)
        {
            case UnlockRequirement.BuildingTimesPurchased:
                if(building == null)
                {
                    Debug.LogError("Select an upgrade");
                }
                else if (building.timesPurchased >= amountToUnlockRequirement && unlocked == false)
                {
                    gameObject.SetActive(true);
                    unlocked = true;
                }
                break;
            case UnlockRequirement.ClicksAmount:
                if(clicksScript.Clicks >= amountToUnlockRequirement && unlocked == false)
                {
                    gameObject.SetActive(true);
                    unlocked = true;
                }
                break;
            case UnlockRequirement.CurrentMoneyAmount:
                if(moneyScript.Money >= amountToUnlockRequirement && !unlocked)
                {
                    gameObject.SetActive(true);
                    unlocked = true;
                }
                break;
            case UnlockRequirement.TotalMoneyAmount:
                break;
        }
    }

    public void OnPurchaseSetUnActive()
    {
        thisButton.gameObject.SetActive(false);
    }
    protected void AddThisUpgradeToUpgradesList()
    {
        UpgradesManager.allUpgrades.Add(this);
    }
    protected virtual void ApplyUpgradeEffect() { }
    public void PurchaseUpgrade()
    {
        if (moneyScript.Money >= cost)
        {
            
            ApplyUpgradeEffect();
            moneyScript.Money = moneyScript.Money - cost;
            publisher.PublishOnIncomeValueChange();
            publisher.PublishOnUpgradePurchase();
            OnPurchaseSetUnActive();
            
        }
    }

}
