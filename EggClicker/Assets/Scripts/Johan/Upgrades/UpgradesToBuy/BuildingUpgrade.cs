using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingUpgrade : UpgradesScript
{
    public enum BuildingType
    {
        None,
        Tent,
        DwarfHelper,
        House
    }
    [Space]
    public BuildingType selectedBuildingType;

    public int multiplier;
    protected override void ApplyUpgradeEffect() { building.Income = building.Income * multiplier; }
    protected override void GetBuildingType()
    {

        switch (selectedBuildingType)
        {
            case BuildingType.None:
                Debug.LogError("No Upgrade Type was selected on " + thisButton.ToString());
                break;
            case BuildingType.Tent:
                building = FindObjectOfType<Tent>();
                break;
            case BuildingType.DwarfHelper:
                building = FindObjectOfType<DwarfHelper>();
                break;
            case BuildingType.House:
                building = FindObjectOfType<House>();
                break;
            default:
                break;
        }
    }
    public void SelectUpgrade()
    {
        buyHandler.GetUpgrade(this);
    }


}
