using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradesManager : MonoBehaviour
{
    public static List<UpgradesScript> allUpgrades = new List<UpgradesScript>();
    MoneyScript moneyScript;
    // Start is called before the first frame update
    void Start()
    {

        moneyScript = FindObjectOfType<MoneyScript>();
        StartCoroutine(UpdateCheckTick());
    }

    // Update is called once per frame
    public void GrayOnNotEnoughMoney()
    {
        for (int i = 0; i < allUpgrades.Count; i++)
        {
            if (allUpgrades[i].cost > moneyScript.Money)
            {
                allUpgrades[i].thisButton.interactable = false;
            }
            else { allUpgrades[i].thisButton.interactable = true; }
        }
    }
    public void CheckUnlockRequiremetns()
    {

        for (int i = 0; i < allUpgrades.Count; i++)
        {
            allUpgrades[i].Unlock();
        }
    }
    private IEnumerator UpdateCheckTick()
    {

        yield return new WaitForSeconds(0.2f);
        while (true)
        {
            CheckUnlockRequiremetns();
            GrayOnNotEnoughMoney();
            yield return new WaitForSeconds(1f);
        }
    }
}
