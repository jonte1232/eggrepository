using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyUpgradeHandler : MonoBehaviour
{
    BuildingUpgrade upgrades;
    [SerializeField] Text text;

    private void Start()
    {
        Publisher.OnUpgradePurchase += UpdateText;
    }
    public void GetUpgrade(BuildingUpgrade upgrade)
    {
        upgrades = upgrade;
        UpdateText();
    }

    public void BuySelectedUpgrade()
    {
        upgrades.PurchaseUpgrade();
        UpdateText();
    }

    private void UpdateText()
    {
        text.text = "Increases the income of " + upgrades.selectedBuildingType.ToString() + " by " + upgrades.multiplier.ToString() + " times" + 
        "\n" + "Cost: " + upgrades.cost + "$";
    }
}
