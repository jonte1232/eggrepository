using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Publisher : MonoBehaviour
{
    public delegate void PublishEventDelegate();
    public static event PublishEventDelegate OnIncomeValueChange;
    public static event PublishEventDelegate OnTimesPurchasedValueChanged;
    public static event PublishEventDelegate OnUpgradePurchase;
    public static event PublishEventDelegate OnBuyBuilding;
    public void PublishOnIncomeValueChange()
    {
        if (OnIncomeValueChange != null)
            OnIncomeValueChange();
    }
    public void PublishOnTimesPurchasedValueChange()
    {
        if(OnTimesPurchasedValueChanged != null)
        {
            OnTimesPurchasedValueChanged();
        }
    }
    public void PublishOnUpgradePurchase()
    {
        if(OnUpgradePurchase != null)
        {
            OnUpgradePurchase();
        }
    }
    public void PublishOnBuyBuilding()
    {
        if(OnBuyBuilding != null)
        {
            OnBuyBuilding();
        }
    }
}
