using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tent : Buildings
{
    [SerializeField] GameObject tent;
    protected override void Start()
    {
        base.Start();
    }
    public override void BuyBuilding()
    {
        base.BuyBuilding();
        if (firstTimePurchseDone == false && timesPurchased == 1)
        {
            tent.SetActive(true);
            firstTimePurchseDone = true;
        }
    }
    public override float SendBuildingData()
    {
        return Value;
    }
    protected override void AddThisBuildingToBuildingsList()
    {
        base.AddThisBuildingToBuildingsList();
    }
}
