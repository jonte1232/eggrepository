using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public abstract class Buildings : MonoBehaviour
{
    public BuildingData _buildingData = null;
    public delegate void OnIncomeValueChangedDelegate();
    public event OnIncomeValueChangedDelegate OnIncomeValueChanged;
    protected bool firstTimePurchseDone = false;
    MoneyScript money;
    public string buildingName;
    public float baseCost;
    public float cost;
    public int timesPurchased = 0;
    [SerializeField]private float _income = 1;
    public float _value;
    public Text text;
    Publisher publisher;
    BuildingBuyHandler buyHandler;
    public float Income
    {
        get { return _income; }
        set 
        { 
            _income = value;
            publisher.PublishOnIncomeValueChange();
        }
    }
    public float Value
    {
        get { return _value; }
        set
        {
            _value = value;
            publisher.PublishOnIncomeValueChange();
        }
    }
    public int TimesPurchased
    {
        get { return timesPurchased; }
        set
        {
            timesPurchased = value;
            

            switch (buildingName)
            {
                case "Tent":
                    _buildingData._tentTimesPurchased = value;
                    break;
                case "House":
                    _buildingData._houseTimesPurchased = value;
                    break;
                case "Dwarf Helper":
                    _buildingData._helperTimesPurchased = value;
                    break;
            }
            
            publisher.PublishOnTimesPurchasedValueChange();
        }
    }

    private void Awake()
    {
        publisher = FindObjectOfType<Publisher>();
        AddThisBuildingToBuildingsList();
    }
    protected virtual void Start()
    {
        _buildingData = FindObjectOfType<BuildingData>();
        buyHandler = FindObjectOfType<BuildingBuyHandler>();
        text = GetComponentInChildren<Text>();
        money = FindObjectOfType<MoneyScript>();
        text.text = buildingName + " -  Cost: " + baseCost;
    }
    public virtual void BuyBuilding()
    {
        if (timesPurchased == 0 && money.Money >= baseCost) 
        {
            TimesPurchased++;
            money.Money = money.Money - baseCost;
            cost = Mathf.Round( baseCost * Mathf.Pow(1.15f, timesPurchased));
            text.text = timesPurchased + "  " + buildingName + " -  Cost: " + cost;
            publisher.PublishOnBuyBuilding();
        }
        else if (money.Money >= cost && timesPurchased > 0)
        {
            TimesPurchased++;
            money.Money = money.Money - cost;
            cost = Mathf.Round(baseCost * Mathf.Pow(1.15f, timesPurchased));
            text.text = timesPurchased + "  " + buildingName + " -  Cost: " + cost;
            publisher.PublishOnBuyBuilding();
        }
        Value = timesPurchased * Income;
        

    }

    public void SelectBuilding()
    {
        buyHandler.GetBuilding(this);
    }

    public virtual float SendBuildingData()
    {
        return Value;
    }
    protected virtual void AddThisBuildingToBuildingsList()
    {

        BuildingsManager.allBuildings.Add(this);
    }
    public void UpdateValue()
    {
        _value = timesPurchased * _income;
    }
}
