using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : Buildings
{
    [SerializeField] GameObject house;
    protected override void Start()
    {
        base.Start();
    }
    public override void BuyBuilding()
    {
        base.BuyBuilding();
        if (firstTimePurchseDone == false && timesPurchased == 1)
        {
            house.SetActive(true);
            firstTimePurchseDone = true;
        }
    }
    public override float SendBuildingData()
    {
        return Value;
    }
    protected override void AddThisBuildingToBuildingsList()
    {
        base.AddThisBuildingToBuildingsList();
    }
}
