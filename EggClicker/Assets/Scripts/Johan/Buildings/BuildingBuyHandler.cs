using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingBuyHandler : MonoBehaviour
{
    Buildings buildings;
    [SerializeField]Text text;

    private void Start()
    {
        Publisher.OnUpgradePurchase += UpdateText;
    }
    public void GetBuilding(Buildings building)
    {
        buildings = building;
        UpdateText();
    }

    public void BuySelectedBuilding()
    {
        buildings.BuyBuilding();
        UpdateText();
    }

    private void UpdateText()
    {
        if(buildings != null)
        {
            text.text = buildings.buildingName + "\n" + "Owned: " + buildings.timesPurchased.ToString() + "\n" +
    "$/s: " + buildings.Value + "\n" + "Each produces " + buildings.Income + " $/s";
        }

    }
}
