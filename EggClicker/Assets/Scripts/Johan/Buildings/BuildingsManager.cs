using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingsManager : MonoBehaviour
{
    [SerializeField] private MoneyData _moneyData = null;
    
    public static List<Buildings> allBuildings = new List<Buildings>();
    public static float moneyPerSecond;

    private void Start()
    {
        Publisher.OnIncomeValueChange += GetMoneyPerSecond;
        Publisher.OnIncomeValueChange += UpdateAllBuildingValues;
        //StartCoroutine(UpdateTick());
    }
    
    public void GetMoneyPerSecond()
    {
        moneyPerSecond = 0;
        for (int i = 0; i < allBuildings.Count; i++)
        {
            moneyPerSecond += allBuildings[i].SendBuildingData();
            _moneyData._moneyPerSecond = moneyPerSecond;
        }

    }
    /*
    IEnumerator UpdateTick()
    {
        while (true)
        {
            for (int i = 0; i < allBuildings.Count; i++)
            {
                allBuildings[i].UpdateValue();
                yield return new WaitForSeconds(0.5f);
            }
        }
    }
    */
    void UpdateAllBuildingValues()
    {
        for (int i = 0; i < allBuildings.Count; i++)
        {
            allBuildings[i].UpdateValue();
        }
    }
    
}
