using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CoinHandler : MonoBehaviour
{
    [SerializeField] Text text;
    private int _coins;

    public int Coins
    {
        get { return _coins; }
        set 
        { 
            _coins = value;
            UpdateText();
        }
    }

    public void UpdateText()
    {
        text.text = "    " + Coins.ToString();
    }
}
