using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PanelButtonManager : MonoBehaviour
{
    bool panelOpening = false;
    Vector3 originalScale = new Vector3(1, 1, 1);
    [SerializeField]public  List<Panel> allPanels = new List<Panel>();
    private void Start()
    {
        Debug.LogError(this);
        for (int i = 0; i < allPanels.Count; i++)
        {
            allPanels[i].gameObject.SetActive(false);
        }
    }

    public void OpenPanel(int i)
    {
        Debug.LogError(this);
        if (allPanels[i].transform.localScale == originalScale)
        {
            ClosePanel();
        }
        else if(panelOpening == false && allPanels[i].transform.localScale != originalScale)
        {
            //StopAllCoroutines();
            allPanels[i].gameObject.SetActive(true);
            StartCoroutine(TweenPanelSize(i));
            for (int x = 0; x < allPanels.Count; x++)
            {
                if (x == i)
                {
                    continue;
                }
                allPanels[x].gameObject.SetActive(false);
            }
        }


    }
    public void ClosePanel()
    {
        Debug.LogError(this);
        for (int i = 0; i < allPanels.Count; i++)
        {
            if(allPanels[i].gameObject == enabled)
            {
                allPanels[i].gameObject.SetActive(false);
                //StopAllCoroutines();
            }
        }
    }

    IEnumerator TweenPanelSize(int i)
    {
        Debug.LogError(this);
        panelOpening = true;
        float floatValue = 0;
        while(floatValue < 1)
        {
            floatValue += 0.05f;
            allPanels[i].transform.localScale = new Vector3(floatValue, floatValue, floatValue);
            yield return new WaitForSeconds(0.01f);
        }
        allPanels[i].transform.localScale = originalScale;
        panelOpening = false;
        yield return null;
    }
}
