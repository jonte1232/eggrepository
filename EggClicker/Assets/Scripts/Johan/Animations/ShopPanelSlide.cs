using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopPanelSlide : MonoBehaviour
{
    [SerializeField] Animator anim;
    public bool isExposed = false;
    BuildingPanelSlide buildingPanel;
    UpgradesPanelSlide upgradesPanel;

    private void Start()
    {
        buildingPanel = FindObjectOfType<BuildingPanelSlide>();
        upgradesPanel = FindObjectOfType<UpgradesPanelSlide>();
    }
    public void Slide()
    {
        if (buildingPanel.isExposed) { buildingPanel.Slide(); }
        if (upgradesPanel.isExposed) { upgradesPanel.Slide(); }
        if (!isExposed)
        {
            anim.SetBool("SlideIn", true);
            isExposed = true;
        }
        else if (isExposed)
        {
            anim.SetBool("SlideIn", false);
            isExposed = false;
        }
    }
}
