using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingPanelSlide : MonoBehaviour
{
    [SerializeField]Animator anim;
    public bool isExposed = false;
    ShopPanelSlide shopPanel;
    private void Start()
    {
        shopPanel = FindObjectOfType<ShopPanelSlide>();
    }
    public void Slide()
    {
        if (shopPanel.isExposed) { shopPanel.Slide(); }
        if (!isExposed)
        {
            anim.SetBool("SlideIn", true);
            isExposed = true;
        }
        else if (isExposed)
        {
            anim.SetBool("SlideIn", false);
            isExposed = false;
        }
    }
}
