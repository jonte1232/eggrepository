using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DwarfPickSpeed : MonoBehaviour
{
    [SerializeField]Animator anim;
    DwarfHelper dwarfHelper;
    Publisher publisher;
    private void Start()
    {
        dwarfHelper = FindObjectOfType<DwarfHelper>();
        Publisher.OnTimesPurchasedValueChanged += SetDwarfPickSpeed;
    }
    void SetDwarfPickSpeed()
    {
        Debug.Log("!");
        anim.speed = 1 + (dwarfHelper.TimesPurchased * 0.01f);
    }
}
