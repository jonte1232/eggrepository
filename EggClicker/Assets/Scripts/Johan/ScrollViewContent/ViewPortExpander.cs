using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
public class ViewPortExpander : MonoBehaviour
{
    [SerializeField]private List<Button> buttonList;
    private float rectHeight = 0;
    private RectTransform rectTransform;
    private float buttonSpacing;
    private void Start()
    {

        buttonSpacing = gameObject.GetComponent<VerticalLayoutGroup>().spacing;
        
    }
    private void OnEnable()
    {

        //buttonList.Clear();
        buttonList = GetComponentsInChildren<Button>().ToList();
        RemoveUnwantedComponents();
        GetContentSize();
        rectTransform = GetComponent<RectTransform>();
        rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, rectHeight);
        
    }
    private void OnDisable()
    {
        rectHeight = 0;
    }

    private void GetContentSize()
    {

        for (int i = 0; i < buttonList.Count; i++)
        {
            //rectHeight += buttonList[i].rect.height + buttonSpacing;
            rectHeight += buttonList[i].GetComponent<RectTransform>().rect.height + buttonSpacing;
        }
    }

    private void RemoveUnwantedComponents()
    {

        for (int i = 0; i < buttonList.Count; i++)
        {
            if (buttonList[i].transform.parent.gameObject != transform.gameObject || buttonList[i].gameObject == transform.gameObject)
            {
                buttonList.RemoveAt(i);
            }
        }
    }
}
