using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyScript : MonoBehaviour
{
    [SerializeField] private MoneyData _moneyData = null;
    
    private float moneyPerSecond;

    public Text moneyText;
    public Text totalMoneyText;
    public Text moneySpentText;
    public Text moneyPerSecondText;

    ClicksScript clicks;
    public float timerUpdate;
    //BuildingsManager BuildingsManager = new BuildingsManager();
    // Start is called before the first frame update
    public float MoneySpent
    {
        get => _moneyData._moneySpent;
        set => _moneyData._moneySpent = value;
    }
    public float TotalMoney
    {
        get => _moneyData._totalMoney;
        set => _moneyData._totalMoney = value;
    }
    public float Money
    {
        get => _moneyData._money;
        set => _moneyData._money = value;
    }
    public float MoneyPerSecond { get => moneyPerSecond; set => moneyPerSecond = value; }

    void Start()
    {

        if (_moneyData == null)
        {
            FindObjectOfType<MoneyData>();
        }
        
        clicks = FindObjectOfType<ClicksScript>();
        clicks.onClickEgg += IncreaseMoney;
        StartCoroutine(UpdateMoneyTick());
    }

    // Update is called once per frame
    void Update()
    {
        //_moneyPerSecond = BuildingsManager.moneyPerSecond;
    }


    void IncreaseMoney()
    {
        _moneyData._money += clicks.MoneyPerClick;
        _moneyData._totalMoney += clicks.MoneyPerClick;
        moneyText.text = "Money:" + _moneyData._money.ToString("F1");
        totalMoneyText.text = "Total Money:" + _moneyData._totalMoney.ToString("F1");
    }

    IEnumerator UpdateMoneyTick()
    {
        
        while (true)
        {
            float totalMoneyPerSecond = BuildingsManager.moneyPerSecond;
            _moneyData._money = _moneyData._money + (totalMoneyPerSecond * 0.1f);
            _moneyData._totalMoney = _moneyData._totalMoney + (totalMoneyPerSecond * 0.1f);

            moneyText.text = "Money:" + _moneyData._money.ToString("F1");
            totalMoneyText.text = "Total Money:" + _moneyData._totalMoney.ToString("F1");

            _moneyData._moneySpent = _moneyData._totalMoney - _moneyData._money;
            moneySpentText.text = "Money Spent:" + _moneyData._moneySpent.ToString("F1");

            moneyPerSecondText.text = "$ " + totalMoneyPerSecond + "/s";
            yield return new WaitForSeconds(0.1f);
        }
    }
}
