using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DwarfGetPos : MonoBehaviour
{
    [SerializeField] Transform objTransform;

    void SetPos()
    {
        transform.position = objTransform.TransformPoint(new Vector3(0,-1.5f, -1f));
    }
}
