using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DwarfParticleEffect : MonoBehaviour
{
    [SerializeField] ParticleSystem particleSystem;

    public void PlayParticleEffect()
    {
        particleSystem.Play();
    }
}
