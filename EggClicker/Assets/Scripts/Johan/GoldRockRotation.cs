using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldRockRotation : MonoBehaviour
{
    [SerializeField, Range(0,0.5f)] float _rotation;
    ClicksScript click;
    Vector3 transformScale;
    // Start is called before the first frame update
    void Start()
    {
        transformScale = transform.localScale;
        click = FindObjectOfType<ClicksScript>();
        click.onClickEgg += ClickTheThing;
    }

    void ClickTheThing()
    {
        transform.localScale = new Vector3(transform.localScale.x * 0.95f, transform.localScale.y * 0.95f, transform.localScale.x * 0.95f);
        Invoke("Resize", 0.1f);
    }
    void Resize()
    {
        transform.localScale = transformScale;
    }
    private void FixedUpdate()
    {
        transform.Rotate(0, _rotation, 0 * Time.fixedDeltaTime);
    }
}
